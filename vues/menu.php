<!-- Ohayon Bryan Bachelor CSI-->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: "Lato", sans-serif;
  background-color: grey;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="ListerAdherents.php">Lister les adherents de la fédération </a>
  <a href="ListerAdherentsnonajour.php">Lister les adherents non à jour de cotisation </a>
  <a href="ListerClub.php">Lister les adherents d'un club par ordre alphabetique des noms</a>
  <a href="ListerAdherentsnonajourAlphabetique.php">Lister les adherents non à jour des cotisations par ordre alpahabetique des noms</a>
  <a href="AjouterAdherent.php">Ajouter un nouvel ahdérent</a>
  <a href="ListerClub2.php">Ajouter un adhérent à un club</a>
  <a href="Camembert.php">Ensemble de la fédération la répartition Homme/ Femme</a>
  <a href="ListerClub3.php"> La répartition Homme/ Femme de chaque club</a>
  <a href="Diagramme.php"> La répartition des âges par tranche de 10 de l’ensemble des adhérents de la fédération</a>
</div>

<div id="main">
  <span style="font-size:30px;cursor:pointer;" onclick="openNav()">&#9776; Acceder au menu</span>
</div>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>
   
</body>
</html> 




