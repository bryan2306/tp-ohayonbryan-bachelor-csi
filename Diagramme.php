<!-- Ohayon Bryan Bachelor CSI-->
<!DOCTYPE html>
<html>
<head>
        <?php
 $repInclude = './include/';
  $repVues = './vues/';
 include($repVues."entete.php") ;
  include($repVues."menu.php") ;
    ?>
	<title>Diagramme à barre</title>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>
</head>
<body>
	<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php

  include('Include/connexion.php') ; 
    $connexion = dbBasket::getInstance();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 0 and 10";
     $res = $connexion->query($req);
     $age = $res->fetch();

    $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 11 and 20";
     $res = $connexion->query($req);
     $age1 = $res->fetch();

    $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 21 and 30";
     $res = $connexion->query($req);
     $age2 = $res->fetch();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 31 and 40";
     $res = $connexion->query($req);
     $age3 = $res->fetch();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 41 and 50";
     $res = $connexion->query($req);
     $age4 = $res->fetch();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 51 and 60";
     $res = $connexion->query($req);
     $age5 = $res->fetch();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 61 and 70";
     $res = $connexion->query($req);
     $age6 = $res->fetch();

     $req = "SELECT count(*) FROM adherents WHERE round(DATEDIFF(CURRENT_DATE,datedenaissance)/365) BETWEEN 71 and 80";
     $res = $connexion->query($req);
     $age7 = $res->fetch();


     $req = "SELECT count(*) FROM adherents";
     $res = $connexion->query($req);
     $total = $res->fetch() ;
     ?>
<script>

var age = <?php echo $age[0] ?>;
var age1 = <?php echo $age1[0] ?>;
var age2 = <?php echo $age2[0] ?>;
var age3 = <?php echo $age3[0] ?>;
var age4 = <?php echo $age4[0] ?>;
var age5 = <?php echo $age5[0] ?>;
var age6 = <?php echo $age6[0] ?>;
var age7 = <?php echo $age7[0] ?>;

var total = <?php echo $total[0]?>;
var prctAge = age * 100  / total;
var prctAge1 = age1 * 100  / total;
var prctAge2 = age2 * 100  / total;
var prctAge3 = age3 * 100  / total;
var prctAge4 = age4 * 100  / total;
var prctAge5 = age5 * 100  / total;
var prctAge6 = age6 * 100  / total;
var prctAge7 = age7 * 100  / total;
// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'La répartition des âges par tranche de 10 de l’ensemble des adhérents de la fédération.'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Pourcentage du nombre d’adherents'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    "series": [
        {
            "name": "Browsers",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Tranche de 0 à 10",
                    "y": prctAge,
                    
                },
                {
                    "name": "Tranche de 10 à 20",
                    "y": prctAge1,
                    
                },
                {
                    "name": "Tranche de 20 à 30 ",
                    "y":prctAge2,
                  
                },
                {
                    "name": "Tranche de 30 à 40",
                    "y": prctAge3,
                    
                },
                {
                    "name": "Tranche de 40 à 50",
                    "y": prctAge4,
                   
                },
                {
                    "name": "Tranche de 50 à 60",
                    "y": prctAge5,
                    
                },
                {
                    "name": "Tranche de 60 à 70",
                    "y": prctAge6,
                
                },
                {
                     "name": "Tranche de 70 à 80",
                    "y": prctAge7,
                   
                }
            ]
        }
    ],
});
</script>
<?php
  include($repVues."pied.php") ;
?>
</body>
</html>