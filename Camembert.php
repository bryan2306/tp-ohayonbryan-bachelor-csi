<!-- Ohayon Bryan Bachelor CSI-->
<!DOCTYPE html>
<html>
<head>
    <?php
 $repInclude = './include/';
  $repVues = './vues/';
 include($repVues."entete.php") ;
  include($repVues."menu.php") ;
    ?>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Camembert Homme/ Femme </title>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
</head>
<body>
	<?php
     
    
	include('Include/connexion.php') ; 
    $connexion = dbBasket::getInstance();

     $req = "SELECT count(*) from adherents where genre = 'F'";
     $res = $connexion->query($req);
     $femmes = $res->fetch() ; 


     $req = "SELECT count(*) from adherents where genre = 'M'";
     $res = $connexion->query($req);
     $hommes = $res->fetch() ;

 
     ?>
	<div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>

<script>

var femmes = <?php echo $femmes[0] ?>;
var hommes = <?php echo $hommes[0] ?>;

var total = hommes + femmes;

var prctHommes = hommes * 100  / total;
var prctfemmes = femmes * 100 / total;

    // Build the chart

Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Camembert dynamique pour donner pour l’ensemble de la fédération la répartition Homme/ Femme '
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Homme',
            y: prctHommes
        }, {
            name: 'Femme',
            y: prctfemmes
        
        }]
    }]
});

</script>
<?php
  include($repVues."pied.php") ;
?>
</body>
</html>