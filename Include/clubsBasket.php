<!-- Ohayon Bryan Bachelor CSI-->
<?php
include('connexion.php') ; 
class ClubsBasket
{
  private $idclubs; 
  private $nomclub;      
  private $codepostal;
  private $ville;
  

  
  public function __construct($idclubs, $nomclub, $codepostal, $ville)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->idclubs = $idclubs;
    $this->nomclub = $nomclub;
    $this->codepostal = $codepostal;
    $this->ville =$ville;
  
  }


  
   public static function getClubsBasket()
   {

        $connexion = dbBasket::getInstance();

         $req = "SELECT * FROM clubs";
         $res = $connexion->query($req);
        $produits = $res->fetchAll() ; 
     return $produits;

   }

    /**
     * @return mixed
     */
    public function getIdclubs()
    {
        return $this->idclubs;
    }

    /**
     * @param mixed $idclubs
     *
     * @return self
     */
    public function setIdclubs($idclubs)
    {
        $this->idclubs = $idclubs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNomclub()
    {
        return $this->nomclub;
    }

    /**
     * @param mixed $nomclub
     *
     * @return self
     */
    public function setNomclub($nomclub)
    {
        $this->nomclub = $nomclub;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * @param mixed $codepostal
     *
     * @return self
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     *
     * @return self
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }
}

?>
