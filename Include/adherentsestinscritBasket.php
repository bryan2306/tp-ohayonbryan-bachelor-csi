<!-- Ohayon Bryan Bachelor CSI-->
<?php
include('connexion.php');
class AdherentsestinscritBasket
{
  private $idadherents; 
  private $idclubs;      
  private $dateinscription;
  private $anneedelicence;

  
  public function __construct($idadherents, $idclubs, $dateinscription, $anneedelicence)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->idadherents = $idadherents;
    $this->idclubs = $idclubs;
    $this->dateinscription = $dateinscription;
    $this->anneedelicence = $anneedelicence;
  
  }
 public static function listerAdherentsclubBasket($nom)
    { 

        $connexion = dbBasket::getInstance();
        $req = "Select adherents.idadherents, adherents.prenom, adherents.nom, datedenaissance, genre from clubs, adherents, adherents_est_inscrit where clubs.idclubs = adherents_est_inscrit.idclubs and adherents.idadherents = adherents_est_inscrit.idadherents and clubs.nom_club = '".$nom."' order by adherents.nom asc";
        $sql = $connexion->query($req);
        $res = $sql->fetchAll();  
        return $res;
    }
  public static function ajouterAdherentsinscritBasket($idadherents, $idclubs, $date_inscription, $annee_de_license)
  {
        $connexion = dbBasket::getInstance();
        $req ="insert into adherents_est_inscrit"
       ."(idadherents,idclubs,date_inscription, annee_de_licence) values ('"
       .$idadherents."','"
       .$idclubs."','"
       .$date_inscription."','"
       .$annee_de_license."');";
        $sql = $connexion->query($req); 
        return $sql;
  }
  
    /**
     * @return mixed
     */
    public function getIdadherents()
    {
        return $this->idadherents;
    }

    /**
     * @param mixed $idadherents
     *
     * @return self
     */
    public function setIdadherents($idadherents)
    {
        $this->idadherents = $idadherents;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdclubs()
    {
        return $this->idclubs;
    }

    /**
     * @param mixed $idclubs
     *
     * @return self
     */
    public function setIdclubs($idclubs)
    {
        $this->idclubs = $idclubs;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateinscription()
    {
        return $this->dateinscription;
    }

    /**
     * @param mixed $dateinscription
     *
     * @return self
     */
    public function setDateinscription($dateinscription)
    {
        $this->dateinscription = $dateinscription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnneedelicence()
    {
        return $this->anneedelicence;
    }

    /**
     * @param mixed $anneedelicence
     *
     * @return self
     */
    public function setAnneedelicence($anneedelicence)
    {
        $this->anneedelicence = $anneedelicence;

        return $this;
    }
}

?>
