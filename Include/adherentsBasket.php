<!-- Ohayon Bryan Bachelor CSI-->
<?php
include('connexion.php') ; 

class AdherentsBasket
{
  private $idadherents; 
  private $prenom;      
  private $nom;
  private $datedenaissance;
  private $genre;

  
  public function __construct($idadherents, $prenom, $nom, $datedenaissance,$genre)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->idadherents = $idadherents;
    $this->prenom = $prenom;
    $this->nom = $nom;
    $this->datedenaissance = $datedenaissance;
    $this->genre =$genre;
  
  }
  
   public static function listerAdherentBasket()
  {

     $connexion = dbBasket::getInstance();

     $req = "SELECT adherents.idadherents, adherents.nom, adherents.prenom, datedenaissance, clubs.nom_club ,adherents_est_inscrit.date_inscription FROM adherents, clubs, adherents_est_inscrit WHERE adherents.idadherents = adherents_est_inscrit.idadherents AND clubs.idclubs = adherents_est_inscrit.idclubs";
    
     $res = $connexion->query($req);
     $produits = $res->fetchAll() ; 
     return $produits;
    }

   public static function listerAdherentnonajourBasket()
  {

     $connexion = dbBasket::getInstance();

     $req = "SELECT DISTINCT adherents.idadherents, nom, prenom, date_inscription,annee_de_licence                     
                    FROM adherents , clubs,adherents_est_inscrit
                    WHERE adherents.idadherents = adherents_est_inscrit.idadherents 
                    AND clubs.idclubs = adherents_est_inscrit.idclubs
                    and year(date_inscription)<year(current_date)";
    
     $res = $connexion->query($req);
     $produits = $res->fetchAll() ; 
     return $produits;
    }

   public static function listerAdherentnonajourAlphabetiqueBasket()
  {
     $connexion = dbBasket::getInstance();

     $req = "SELECT DISTINCT adherents.idadherents, nom, prenom, date_inscription,annee_de_licence                     
                    FROM adherents , clubs,adherents_est_inscrit
                    WHERE adherents.idadherents = adherents_est_inscrit.idadherents 
                    AND clubs.idclubs = adherents_est_inscrit.idclubs
                    and year(date_inscription)<year(current_date)
                    order by nom ASC";
    
     $res = $connexion->query($req);
     $produits = $res->fetchAll() ; 
     return $produits;
    }

     public static function AjouterAdherentBasket($idadherents, $prenom, $nom, $datedenaissance,$genre)
  {
    $connexion = dbBasket::getInstance();
        $req="insert into adherents"
       ."(idadherents,prenom,nom, datedenaissance, genre) values ('"
       .$idadherents."','"
       .$prenom."','"
       .$nom."','"
       .$datedenaissance."','"
       .$genre."');";
        $res = $connexion->query($req); 
        return $res;
  }

    /*

    /**
     * @return mixed
     */
    public function getIdadherents()
    {
        return $this->idadherents;
    }

    /**
     * @param mixed $idadherents
     *
     * @return self
     */
    public function setIdadherents($idadherents)
    {
        $this->idadherents = $idadherents;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     *
     * @return self
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     *
     * @return self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDatedenaissance()
    {
        return $this->datedenaissance;
    }

    /**
     * @param mixed $datedenaissance
     *
     * @return self
     */
    public function setDatedenaissance($datedenaissance)
    {
        $this->datedenaissance = $datedenaissance;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     *
     * @return self
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }
}

?>
