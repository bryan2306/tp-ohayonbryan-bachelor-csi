-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 21 mars 2019 à 12:42
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sport`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherents`
--

DROP TABLE IF EXISTS `adherents`;
CREATE TABLE IF NOT EXISTS `adherents` (
  `idadherents` int(10) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(45) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `datedenaissance` date NOT NULL,
  `genre` enum('M','F') NOT NULL DEFAULT 'M',
  PRIMARY KEY (`idadherents`)
) ENGINE=MyISAM AUTO_INCREMENT=8798799 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adherents`
--

INSERT INTO `adherents` (`idadherents`, `prenom`, `nom`, `datedenaissance`, `genre`) VALUES
(1, 'bryan', 'ohayon', '1997-06-23', 'M'),
(2, 'alan', 'toledana', '1996-09-23', 'M'),
(3, 'ruben', 'hababou', '1997-01-31', 'M'),
(4, 'joyce', 'elamaleh', '1997-04-13', 'F'),
(5, 'aurelie', 'zenou', '1999-03-29', 'F'),
(9, 'dylan', 'azoulay', '1997-04-19', 'M'),
(10, 'ilana', 'guez', '1997-10-15', 'F'),
(11, 'sandra', 'ohayon', '1988-06-01', 'F'),
(12, 'arie', 'levy', '1997-10-08', 'M'),
(98, 'bryan', 'levy', '2019-03-08', 'M'),
(20, 'sephora', 'arzoune', '1997-03-01', 'F'),
(22, 'Jean', 'Luc', '2019-03-03', 'M'),
(89, 'kim', 'uzan', '1955-05-06', 'F'),
(26, 'samuel', 'ohayon', '1977-06-25', 'M'),
(789, 'joao', 'lima', '1991-10-01', 'M');

-- --------------------------------------------------------

--
-- Structure de la table `adherents_est_inscrit`
--

DROP TABLE IF EXISTS `adherents_est_inscrit`;
CREATE TABLE IF NOT EXISTS `adherents_est_inscrit` (
  `idadherents` int(10) NOT NULL,
  `idclubs` int(10) NOT NULL,
  `date_inscription` date NOT NULL,
  `annee_de_licence` varchar(4) NOT NULL,
  PRIMARY KEY (`idadherents`,`idclubs`),
  KEY `clubs_idclubs` (`idclubs`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adherents_est_inscrit`
--

INSERT INTO `adherents_est_inscrit` (`idadherents`, `idclubs`, `date_inscription`, `annee_de_licence`) VALUES
(1, 1, '2001-06-04', '2002'),
(1, 2, '1998-11-06', '2000'),
(3, 4, '2005-01-03', '2009'),
(4, 3, '1996-12-06', '2008'),
(1, 5, '2019-02-06', '2019'),
(3, 1, '2019-03-04', '1555'),
(22, 1, '2017-02-12', '2019'),
(789, 1, '2018-09-27', '2017'),
(26, 2, '1987-07-26', '1900'),
(4, 1, '2018-03-04', '2002');

-- --------------------------------------------------------

--
-- Structure de la table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
CREATE TABLE IF NOT EXISTS `clubs` (
  `idclubs` int(10) NOT NULL AUTO_INCREMENT,
  `nom_club` varchar(45) NOT NULL,
  `codepostal` varchar(45) NOT NULL,
  `ville` varchar(45) NOT NULL,
  PRIMARY KEY (`idclubs`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `clubs`
--

INSERT INTO `clubs` (`idclubs`, `nom_club`, `codepostal`, `ville`) VALUES
(1, 'nba', '95200', 'sarcelles'),
(2, 'cabourg', '75000', 'Paris'),
(3, 'sacramentos', '06000', 'nice'),
(4, 'evian', '95350', 'saint-brice'),
(5, 'aass', '69000', 'lyon');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
